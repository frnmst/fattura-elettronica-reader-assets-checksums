# fattura-elettronica-reader-assets-checksums

Track file changes for assets files used in fattura-elettronica-reader.

## Table of contents

<!--TOC-->

- [fattura-elettronica-reader-assets-checksums](#fattura-elettronica-reader-assets-checksums)
  - [Table of contents](#table-of-contents)
  - [Description](#description)
  - [License](#license)
  - [Changelog and trusted source](#changelog-and-trusted-source)
  - [Crypto donations](#crypto-donations)

<!--TOC-->

## Description

The
[copyright statement from the FatturaPA website](https://www.fatturapa.gov.it/it/copyright/index.html)
prevents me from including the schemas, XSLTs and all the other needed files
in
[fattura-elettronica-reader](https://blog.franco.net.eu.org/software/#fattura-elettronica-reader).

As an alternative you can find the files on:

- [the official FatturaPA documentation page](https://www.fatturapa.gov.it/it/norme-e-regole/documentazione-fattura-elettronica/formato-fatturapa/)
- [agid.gov.it](https://eidas.agid.gov.it/TL/TSL-IT.xml)

This repository is automatically upated by a script once a day to track changes.

## License

Copyright (C) 2021-2022 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)

fattura-elettronica-reader-assets-checksums is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

fattura-elettronica-reader-assets-checksums is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with fattura-elettronica-reader-assets-checksums.  If not, see <http://www.gnu.org/licenses/>.

## Changelog and trusted source

You can check the authenticity of new releases using my public key.

Changelogs, instructions, sources and keys can be found at [blog.franco.net.eu.org/software](https://blog.franco.net.eu.org/software/).

## Crypto donations

- Bitcoin: bc1qnkflazapw3hjupawj0lm39dh9xt88s7zal5mwu
- Monero: 84KHWDTd9hbPyGwikk33Qp5GW7o7zRwPb8kJ6u93zs4sNMpDSnM5ZTWVnUp2cudRYNT6rNqctnMQ9NbUewbj7MzCBUcrQEY
- Dogecoin: DMB5h2GhHiTNW7EcmDnqkYpKs6Da2wK3zP
- Vertcoin: vtc1qd8n3jvkd2vwrr6cpejkd9wavp4ld6xfu9hkhh0
