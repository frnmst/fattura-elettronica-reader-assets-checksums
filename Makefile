#!/usr/bin/env make
#
# Makefile
#
# Copyright (C) 2021 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of fattura-elettronica-reader-assets-checksums.
#
# fattura-elettronica-reader-assets-checksums is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fattura-elettronica-reader-assets-checksums is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fattura-elettronica-reader-assets-checksums.  If not, see <http://www.gnu.org/licenses/>.
#

default: install-dev

install-dev:
	pipenv install --dev
	pipenv run pre-commit install
	pipenv graph
	pipenv check

uninstall-dev:
	rm -f Pipfile.lock
	pipenv --rm

update: install-dev
	pipenv run pre-commit autoupdate

.PHONY: install-dev uninstall-dev update
